#!/usr/bin/perl

# Run from kohaclone

use Modern::Perl;

use utf8::all;
use Time::Moment;
use Lingua::EN::Numbers::Ordinate;
use IO::Prompt::Tiny qw/prompt/;
use List::MoreUtils qw(uniq);
use YAML qw(LoadFile DumpFile);
use List::Util qw/any/;
use File::Basename;

my $rootdir = dirname(__FILE__) . "/../";

# Load config
my $configfile = $rootdir . 'etc/config.yaml';
my $config     = LoadFile($configfile);

# Get all commits from git
my @commits =
`git -c i18n.logOutputEncoding=UTF-8 log --pretty=format:"%H, %ct, %at, %an, %ae"`;
unless (@commits) {
    say 'Hum... no commits?';
    exit 1;
}

# Convert commit lines to hashref keyed on commitref
# and record name -> email, email -> name, commitdate
my $seen_name  = {};
my $seen_email = {};
my $commitref  = {};
for my $commit (@commits) {
    chomp $commit;
    my ( $commit_id, $cdate, $adate, $name, $email ) = split( /,\s/, $commit );
    $name = lc($name);
    $name =~ s/([^\s\w]*)(\S+)/$1\u\L$2/g;
    $name =~ s/^\s+|\s+$//g;
    $email = lc($email);
    $email =~ s/^\s+|\s+$//g;
    $seen_name->{$name}->{$email}  = 1;
    $seen_email->{$email}->{$name} = 1;
    $commitref->{$commit_id} =
      { name => $name, email => $email, cdate => $cdate, adate => $adate };
}

# Construct authors hashmap email + name = author and save to file so we don't need to
# do manual disambiguation, which prompts for user input, for developers we've already
# seen.
my $authorsfile = $rootdir . 'etc/dev_map.yaml';
my $authors     = LoadFile($authorsfile);
for my $commit (
    sort { $a->{cdate} <=> $b->{cdate} || $a->{adate} <=> $b->{adate} }
    ( values %{$commitref} ) )
{
    my $name  = $commit->{name};
    my $email = $commit->{email};
    next if ( $name eq '=' || $email eq '=' );
    if ( !defined( $authors->{$email}->{$name} ) ) {
        print "\n\nNew developer found with email $email and name $name\n";
        print "cdate: $commit->{cdate}\n";
        print "Previous name matches: "
          . join( " ", ( keys %{ $seen_name->{$name} } ) ) . "\n";
        print "Previous email matches: "
          . join( " ", ( keys %{ $seen_email->{$email} } ) ) . "\n";
        my $author =
          prompt( "Please enter their name for disambiguation:", "$name" );
        $authors->{$email}->{$name} = $author;

        # Save progress
        DumpFile( $authorsfile, $authors );
    }
}
DumpFile( $authorsfile, $authors );

# Create alias map from $authors
my $aliases = {};
for my $email ( keys %{$authors} ) {
    for my $name ( keys %{ $authors->{$email} } ) {
        $aliases->{$name} = $authors->{$email}->{$name};
        $aliases->{ $authors->{$email}->{$name} } = $authors->{$email}->{$name};
    }
}

# Construct contributors hashref keyed on contributor name
# { contributor_name =>
#   { first_commit => epoch_date, commits => number_of_commits }
# }
my $contributorsfile = 'docs/contributors.yaml';
my $contributors     = LoadFile($contributorsfile);
my $seen;
my $committer = 1;
for my $commit (
    sort { $a->{cdate} <=> $b->{cdate} || $a->{adate} <=> $b->{adate} }
    ( values %{$commitref} ) )
{
    my $commit_name  = $commit->{name};
    my $commit_email = $commit->{email};
    next if ( $commit_name eq '=' || $commit_email eq '=' );

    my $author = $authors->{$commit_email}->{$commit_name};
    if ( $contributors->{$author} ) {

        # Reset first_commit and commit count if 'first_commit'
        if ( !defined( $seen->{$author} ) ) {
            $contributors->{$author}->{first_commit} = $commit->{cdate};
            $contributors->{$author}->{commits}      = 0;
            $seen->{$author}                         = 1;

            # Set commiter number
            $contributors->{$author}->{committer} = $committer++;
        }

        # Increment author commits
        $contributors->{$author}->{commits}++;
    }
    else {

        # Set first commit date
        $contributors->{$author}->{first_commit} = $commit->{cdate};

        # Set first commit increment
        $contributors->{$author}->{commits} = 1;

        # Set commiter number
        $contributors->{$author}->{committer} = $committer++;
    }
}

## Merge history and authors
my $history = {};
if ( open( my $file, "<:encoding(UTF-8)", "docs/history.txt" ) ) {
    my @lines;
    while ( my $line = <$file> ) {
        chomp $line;
        push @lines, $line;
    }
    close($file);

    shift @lines;    #remove header row
    foreach (@lines) {
        my ( $epoch, $date, $desc, $tag ) = split(/\t/);
        if ( defined( $history->{$epoch} ) ) {
            say "Duplicate epoch in history found, but that's OK: $epoch";
        }
        $desc =~ s/^\s+|\s+$//g;

        # Reprint original history with cleaned whitespace
        #my $line = $epoch . "\t" . $date . "\t" . $desc;
        #$line .= "\t" . $tag if ( defined($tag) );
        #$line .= "\n";
        #print $line;

        # Map Contributors
        ($desc, $epoch, $tag) = _get_contributor( $desc, $epoch, $tag );

        #my $tm = Time::Moment->from_epoch($epoch);
        #$date = $tm->strftime("%B %e %Y");
        push @{ $history->{$epoch} },
          {
            date        => $date,
            description => $desc,
            tag         => $tag
          };
    }
}

# Merge contributors found in history file
sub _get_contributor {
    my ( $desc, $epoch, $tag ) = (@_);
    if ( $desc =~ /^(.*)\sbecomes\sthe\s\d.*$/ ) {
        my $author = $1;
        $author =~ s/Katipo's new developer //g;
        $author =~ s/Koha production\?\?\?/Anonymous/g;

        if ( defined( $aliases->{$author} ) ) {

            #print "author: " . $author;
            my $match = quotemeta($author);

            #print "\t=>\t" . $match;
            #print "\t=>\t" . $aliases->{$author} . "\n";
            $desc =~ s/$match/$aliases->{$author}/g;
            $author = $aliases->{$author};
        }

        #print "Contributor line found for $author\n";
        if ( $contributors->{$author} ) {
            #print "Match found, checking first_commit\n";
            if ( $contributors->{$author}->{first_commit} != $epoch ) {

                #print "Date doesn't match\n";
                $epoch = $contributors->{$author}->{first_commit};
                #my $which = prompt(
                #    "Which first_commit should we use 0: "
                #      . $contributors->{$author}->{first_commit} . " 1: "
                #      . $epoch,
                #    0
                #);
                #my $which = 0;
                #$contributors->{$author}->{first_commit} =
                #  $which ? $epoch : $contributors->{$author}->{first_commit};
            } else {
                #print "Date matched\n";
            }
        }
        else {
            #print "No match found, adding to \$contributors\n";
            $contributors->{$author}->{first_commit} = $epoch;
            $contributors->{$author}->{commits}      = 1;
        }
        
        $tag = 'developer';
        $desc =~ s/Katipo's new developer //g;
        $desc =~ s/Koha production\?\?\?/Anonymous/g;
    }

    return ($desc, $epoch, $tag);
}
DumpFile( $contributorsfile, $contributors );

# Merge roles into crontributors
for my $contributor ( keys %{$contributors} ) {
    delete $contributors->{$contributor}->{roles}
      if exists $contributors->{$contributor}->{roles};
}
for my $version ( keys %{ $config->{team} } ) {
    for my $role ( keys %{ $config->{team}->{$version} } ) {
        my $normalized_role = "$role";
        $normalized_role =~ s/s$//;
        if ( ref( $config->{team}->{$version}->{$role} ) eq 'ARRAY' ) {
            for my $contributor ( @{ $config->{team}->{$version}->{$role} } ) {
                my $name = $contributor->{name};
                if ( !exists( $contributors->{$name} ) ) {
                    print $name . " in " . $role . " not found\n";
                }
                unless (
                    any { $_ eq $version }
                    @{ $contributors->{$name}->{roles}->{$normalized_role} }
                  )
                {

                    push @{ $contributors->{$name}->{roles}->{$normalized_role}
                      },
                      $version;
                }
                @{ $contributors->{$name}->{roles}->{$normalized_role} } =
                  sort { $a <=> $b }
                  @{ $contributors->{$name}->{roles}->{$normalized_role} };
            }
        }
        else {
            my $name = $config->{team}->{$version}->{$role}->{name};
            if ( !exists( $contributors->{$name} ) ) {
                print $name . " in " . $role . " not found\n";
            }
            unless (
                any { $_ eq $version }
                @{ $contributors->{$name}->{roles}->{$normalized_role} }
              )
            {
                push @{ $contributors->{$name}->{roles}->{$normalized_role} },
                  $version;
            }
            @{ $contributors->{$name}->{roles}->{$normalized_role} } =
              sort { $a <=> $b }
              @{ $contributors->{$name}->{roles}->{$normalized_role} };
        }
    }
}
DumpFile( $contributorsfile, $contributors );

sub line_from_contrib {
    my $contributor = shift;
    my $tm =
      Time::Moment->from_epoch( $contributors->{$contributor}->{first_commit} );
    my $date = $tm->strftime("%B %e %Y");
    my $desc = $contributor . " becomes the 1st";
    $desc .= " committer to Koha"
      if ( $contributors->{$contributor}->{first_commit} < 1274054400 );
    $desc .= " committer to have a patch accepted"
      if ( ( $contributors->{$contributor}->{first_commit} > 1274054400 )
        && ( $contributors->{$contributor}->{first_commit} < 1290211200 ) );
    $desc .= " developer to have a patch pushed"
      if ( $contributors->{$contributor}->{first_commit} > 1290211200 );

    return ( $date, $desc );
}

# Merge contributors into the history hash
for my $contributor (
    sort {
        $contributors->{$a}->{first_commit}
          <=> $contributors->{$b}->{first_commit}
          || $contributors->{$a}->{committer}
          <=> $contributors->{$b}->{committer}
    } grep { exists( $contributors->{$_}->{first_commit} ) }
    ( keys %{$contributors} )
  )
{
#for my $contributor ( keys %{$contributors} ) {
    if ( exists( $contributors->{$contributor}->{first_commit} ) ) {
        if (
            defined(
                $history->{ $contributors->{$contributor}->{first_commit} }
            )
          )
        {
            my $lines =
              $history->{ $contributors->{$contributor}->{first_commit} };
            my $contributor_seen = 0;
            for my $line ( @{$lines} ) {
                if ( $line->{description} =~ /^(.*)\sbecomes\sthe\s\d.*$/ ) {
                    my $author = $1;

                    if ( $author eq $contributor ) {
                        $contributor_seen++;
                    }
                }
            }
            if ( !$contributor_seen ) {
                my ( $date, $desc ) = line_from_contrib($contributor);
                push
                  @{ $history->{ $contributors->{$contributor}->{first_commit} }
                  },
                  { date => $date, description => $desc, tag => 'developer' };
                #print "\n\nFound history lines for epoch: ". $contributors->{$contributor}->{first_commit}. "\n";
                #print "Added new line\n";
                #for my $line ( @{ $history->{ $contributors->{$contributor}->{first_commit} }} ) {
                #    print $line->{description} . "\n";                
                #}
            }
        }
        else {
            my ( $date, $desc ) = line_from_contrib($contributor);
            push @{ $history->{ $contributors->{$contributor}->{first_commit} } },
              { date => $date, description => $desc, tag => 'developer' };
        }
    }
}

# Print combined history
my $dev_number  = 0;
my $histfile;
unless ( open( $histfile, ">:encoding(UTF-8)", "docs/history_new.txt" ) ) {
    warn "Failed to open filehandle\n";
}

print( $histfile "Epoch	Date	Description	Tags\n" );
my $authors_seen;
for my $epoch ( sort { $a <=> $b } ( keys %{$history} ) ) {
    my $lines = $history->{$epoch};
    for my $line ( @{$lines} ) {
        if ( $line->{description} =~ /^(.*)\sbecomes\sthe\s\d.*$/ ) {
            my $author = $1;
            push @{ $authors_seen->{ lc($author) } }, $epoch . " " . $line->{description};
            if ( scalar @{ $authors_seen->{ lc($author) } } > 1 ) {
                print "Extra sighting of " . $author . "\n";
                for my $desc ( @{ $authors_seen->{ lc($author) } } ) {
                    print $desc . "\n";
                }
                print "\n";
                next;
            }
            next if ( $author eq "Anonymous" );
            next if ( $author eq "QA Manager" );
            next if ( $author eq "Koha production???" );
            next if ( $author eq "PTFS Team" );
            next if ( $author eq "Biblibre Team" );
            next if ( $author eq "ByWater Solutions Team" );
            next if ( $author eq "Koha Translators" );
            next if ( $author eq "Koha Instance Knakk-koha" );
            $dev_number++;
            $dev_number = 5
              if ( $author eq 'Steve Tonnesen' );    # Steve is a fixed point
            my $nth = ordinate($dev_number);
            $line->{description} =~ s/(\d+\w\w)/$nth/;
            $line->{tag} = 'developer';
        }

        my $new_line =
          $epoch . "\t" . $line->{date} . "\t" . $line->{description};
        $new_line .= "\t" . $line->{tag} if ( defined( $line->{tag} ) );
        $new_line .= "\n";

        print( $histfile $new_line );
    }
}
close($histfile);

exit 0;
